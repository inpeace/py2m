from pymongo import MongoClient
from pygit2 import RemoteCallbacks, Keypair, clone_repository, Signature
from bson import json_util
from tempfile import mkdtemp
from shutil import rmtree

db = MongoClient().test

git_user = "Narankhuu"
git_email = "narankhuu.b@gmail.com"
git_commit = "auto updated"
git_url = "ssh://git@github.com/naagii/py2m"
git_credential_cb = RemoteCallbacks(credentials = Keypair("git", "ssh_key.pub", "ssh_key", ""))

def clone_repo(repo_url):
  repo_path = mkdtemp()
  return [clone_repository(repo_url, repo_path, callbacks = git_credential_cb), repo_path]

def gh2mongo():
  """
  Check out a copy of the GitHub repo into a temp directory. 
  For each .json file in the repo, create or replace a collection 
  in MongoDB whose name is the same as the .json file with the 
  contents of the .json file. For example, create or replace the 
  contents of my_collection with the contents of my_collection.json.
  """
  
  # clone repo to tmp folder
  repo, path = clone_repo(git_url)

  # replace db.collections with json content
  for file in [f.name for f in repo.revparse_single('master').tree]:
    if file.endswith(".json"): 
      db[file[:-5]].delete_many({})  
      db[file[:-5]].insert_many(json_util.loads(open(path + "/" + file).read()))
  
  # clear tmp directory
  rmtree(path)

def mongo2gh():
  """
  Check out a copy of the GitHub repo into a temp directory. 
  For each collection, update or replace the existing .json file with 
  the items in the collection. For example, create or replace the 
  contents of 'my_collection.json' with the records inside the MongoDB 
  collection "my_collection". Commit the new/changed .json files and push to GitHub.
  """
  # clone repo to tmp folder
  repo, path = clone_repo(git_url)

  refs = "refs/heads/master"
  author = Signature(git_user, git_email)

  # create/replace collection_name.json files.
  for name in db.collection_names():
    file = open(path + "/" + name + ".json", "w")
    file.write(json_util.dumps([i for i in db[name].find()], sort_keys = True, indent = 2, default = json_util.default))
    file.close()
  
  # git add .
  index = repo.index
  index.add_all()
  index.write()
  
  # git commit -m "auto updated"
  repo.create_commit(refs, author, author, git_commit, index.write_tree(), [repo.head.target])

  # git push origin master
  repo.remotes["origin"].push([refs], callbacks=git_credential_cb)

  # clear tmp directory
  rmtree(path)


#mongo2gh()
#gh2mongo()